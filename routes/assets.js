const express = require('express');
const router = express.Router();

const Asset = require('../models/Asset');

// Get all assets
router.get('/', async (req, res) => {
  try {
    const assets = await Asset.find().populate('category');;
    res.json(assets);
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
});

// Create a new asset
router.post('/', async (req, res) => {
  const asset = new Asset(req.body);

  try {
    const newAsset = await asset.save();
    res.status(201).json(newAsset);
  } catch (error) {
    res.status(400).json({ message: error.message });
  }
});

// Get a specific asset by ID
router.get('/:id', getAssetById, (req, res) => {
  res.json(res.asset);
});

// Update a specific asset by ID
router.put('/:id', getAssetById, async (req, res) => {
  try {
    Object.assign(res.asset, req.body);
    const updatedAsset = await res.asset.save();
    res.json(updatedAsset);
  } catch (error) {
    res.status(400).json({ message: error.message });
  }
});

// Delete a specific asset by ID
router.delete('/:id', getAssetById, async (req, res) => {
  try {
    await res.asset.remove();
    res.json({ message: 'Asset deleted' });
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
});

// Middleware to find an asset by ID
async function getAssetById(req, res, next) {
  let asset;
  try {
    asset = await Asset.findById(req.params.id).populate('category');
    if (!asset) {
      return res.status(404).json({ message: 'Asset not found' });
    }
  } catch (error) {
    return res.status(500).json({ message: error.message });
  }

  res.asset = asset;
  next();
}

module.exports = router;