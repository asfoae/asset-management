const mongoose = require('mongoose');

const AssetSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
  },
  category: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Category',
    required: true,
  },
  value: {
    type: Number,
    required: true,
  },
  purchaseValue: {
    type: Number,
    required: true,
  },
  location: String,
  acquisitionDate: Date,
  additionalInfo: Object,
});

module.exports = mongoose.model('Asset', AssetSchema);